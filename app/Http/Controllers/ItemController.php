<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    // Get the list of items of database
    public function index(){
        return response()->json(Item::orderBy('order','ASC')->get());
    }

    // Update item in a edit action
    public function update(Request $request, Item $item)
    {
        if(isset($request->image)){
            $data = $request->image;
            if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
                $data = substr($data, strpos($data, ',') + 1);
                $type = strtolower($type[1]); // jpg, png, gif

                if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
                    $result = ['message'=>'Is not valid type of image','result'=>false];
                }

                $data = base64_decode($data);

                if ($data === false) {
                    $result = ['message'=>'base64_decode failed','result'=>false];
                }
            } else {
                $result = ['message'=>'did not match data URI with image data '.$data,'result'=>false];
            }
            if(!isset($result['result'])){
                define('UPLOAD_DIR', 'images/');
                $filename = UPLOAD_DIR . uniqid().".{$type}";
                if(file_put_contents($filename, $data)){
                    $item->image = $filename;
                }else{
                    $result = ['message'=>'Error upload image','result'=>false];
                }
            }
        }
        if(!isset($result['result'])) {
            $item->description = $request->description;
            $item->save();
            $result = ['message' => 'Item updated', 'result' => true];
        }
        return response()->json($result);
    }

    // Save a new item
    public function store(Request $request)
    {
        $order = Item::max('order');
        if(!$order){
            $order = 1;
        }else{
            $order++;
        }
        $data = $request->image;
        if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
            $data = substr($data, strpos($data, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif

            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
                $result = ['message'=>'Is not valid type of image','result'=>false];
            }

            $data = base64_decode($data);

            if ($data === false) {
                $result = ['message'=>'base64_decode failed','result'=>false];
            }
        } else {
            $result = ['message'=>'did not match data URI with image data '.$data,'result'=>false];
        }
        if(!isset($result['result'])){
            define('UPLOAD_DIR', 'images/');
            $filename = UPLOAD_DIR . uniqid().".{$type}";
            if(file_put_contents($filename, $data)){
                $item = new Item();
                $item->description = $request->description;
                $item->order = $order;
                $item->image = $filename;
                $item->save();
                $result = ['message'=>'Item saved','result'=>true];
            }else{
                $result = ['message'=>'Error upload image','result'=>false];
            }
        }
        return response()->json($result);
    }

    // Delete a items
    public function destroy(Item $item)
    {
        if(unlink(public_path($item->image))){
            try {
                $item->delete();
                $result = ['message'=>'Item deleted','result'=>true];
            } catch (\Exception $e) {
                $result = ['message'=>$e,'result'=>false];
            }
        }else{
            $result = ['message'=>'Error with deleting file','result'=>false];
        }
        return response()->json($result);
    }

    public function changeOrder(Request $request,Item $item){
        $order = 1;
        foreach ($request->newList as $id){
            if($id!=""){
                $i = Item::findOrFail($id);
                $i->order = $order;
                $i->save();
                $order++;
            }
        }
        return response()->json(['message'=>'Item change OK']);
    }
}
