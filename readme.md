# Item List

## Previous requirements

- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- composer
- MySql or MariaDB server

## Instalation

- Once the repository is cloned, go to the project folder (```/item-list```)
- Execute the following command in the terminal: ```composer install```
- Finished the process verify if the .env file was created, if not execute this in terminal: ```cp .env.example .env```
- If you had to create the .env file execute the following in the terminal: ```php artisan key:generate```
- Edit the .env file and configure database:
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=[database name]
DB_USERNAME=[databse user]
DB_PASSWORD=[databse password]
```
- Create the database in mysql with terminal or phpmyadmin
- Execute the following command in the terminal: ```php artisan migrate```

## Execution

If you clone the repository in your apache root folder yo go to browser in ```http://localhost/item-list/public/```

Or you can run ```php artisan serve``` in terminal