// Global Variables
var imageIsOk = false;
var _URL = window.URL || window.webkitURL;

$(document).ready(function () {
    // Get a list of items
    loadItems();
});

function loadItems(){
    $('#list').html(' ');
    $.ajax({url:'items/'})
        .done(function (response) {
            var output = '';
            $.each(response,function (i,v) {
                output+='<li class="list-group-item" draggable="true" id="'+v.id+'" data-order="'+v.order+'">' +
                        '   <div class="row">' +
                        '       <div class="col-4"><img src="'+v.image+'" alt="" width="100"></div>' +
                        '       <div class="col-4"><p>'+v.description+'</p></div>' +
                        '       <div class="col-2">' +
                        '           <button class="btn btn-default edit" data-id="'+v.id+'" data-image="'+v.image+'" data-description="'+v.description+'"><i class="fa fa-edit" aria-hidden="true"></i></button>' +
                        '           <button class="btn btn-danger delete" data-id="'+v.id+'"><i class="fa fa-trash" aria-hidden="true"></i></button>' +
                        '       </div>' +
                        '   </div>' +
                        '</li>';
            });
            $('#list').html(output);

            $("#list").sortable({
                start: function (event, ui) {
                    $(ui.item).data("startindex", ui.item.index());
                },
                stop: function (event, ui) {
                    var $item = ui.item;
                    var startIndex = $item.data("startindex") + 1;
                    var newIndex = $item.index() + 1;
                    if (newIndex != startIndex) {
                        changeOrder(ui.item.attr('id'), $('#list').sortable('toArray'));
                    }
                }
            }).disableSelection();


            $('#counter').html(response.length);

            $(document).off("submit").on('submit','#save-form',function (e) {
                saveForm(e);
            });

            $(document).off("change").on("change","#image",function (e) {
                changeImage(e);
            });

            $(document).off("click").on("click",".edit",function (e) {
                editForm(e);
            });

            $('.delete').off("click").on('click',function (e) {
                console.log(e,$(this))
                var element = e.currentTarget;
                $('.modal').modal('show');
                $(document).off("click").on('click','#delete-item',function(e){
                    deleteItem(element);
                });
            });
        }).fail(function (error) {
            console.log(error)
            $('body').html(error.responseText);
        });
}

// Save a item
function saveForm(e){
    e.preventDefault();
    var descIsOk = e.target.description.value.length <= 300;
    var file = e.target.image.files[0];
    var reader = new FileReader();
    reader.onloadend = function() {
        var data = new FormData();
        data.append('description',e.target.description.value);
        data.append('image',reader.result);
        if(imageIsOk && descIsOk){  // Validate if a image have required dimensions and description have less than 300 characters
            $.ajax({
                url: 'items',
                type: 'POST',
                contentType: false,
                cache: false,
                processData:false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: data
            }).done(function (response) {
                if(response.result){
                    toastr.success(response.message);
                    $('#save-form')[0].reset();
                    $('#prev-image')[0].src = '';
                    loadItems();
                }else{
                    toastr.error(response.message);
                }
            }).fail(function (error) {
                console.log(error);
                $('body').html(error.responseText);
            });
        }else{
            if(!imageIsOk){
                toastr.error('Please select a valid image');
            }
            if(!descIsOk){
                toastr.error('The text only have a 300 characters.');
            }else{
                toastr.error('Please correct errors');
            }
        }
    };
    reader.readAsDataURL(file);
}

// Transform the item into a form to edit that item
function editForm(e){
    // Prevent generate other edit form
    if($('#edit-form').length<1) {
        var output = '<form id="edit-form" class="row" enctype="multipart/form-data">' +
            '   <div class="col-6">' +
            '       <img src="' + e.currentTarget.dataset.image + '" id="new-prev-image" alt="" width="100">' +
            '       <input type="file" class="form-control" name="image" id="new-image"  accept="image/*">' +
            '       <p class="hidden text-danger"></p>' +
            '   </div>' +
            '   <div class="col-4"><textarea class="form-control" name="description">' + e.currentTarget.dataset.description + '</textarea></div>' +
            '   <div class="col-2">' +
            '       <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i></button>' +
            '   </div>' +
            '   <input type="hidden" name="id" value="' + e.currentTarget.dataset.id + '">' +
            '</form>';

        $('#' + e.currentTarget.dataset.id).html(output);

        $('#new-image').off("change").change(function (e) {
            changeImage(e);
        });

        $(document).off("submit").on('submit', '#edit-form', function (e) {
            e.preventDefault();
            var file = e.currentTarget.image.files[0];
            if (file) {
                var reader = new FileReader();
                reader.onloadend = function () {
                    if (imageIsOk) {
                        $.ajax({
                            url: 'items/' + e.currentTarget.id.value,
                            type: 'PUT',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {description: e.currentTarget.description.value, image: reader.result}
                        }).done(function (response) {
                            if (response.result) {
                                toastr.success(response.message);
                                e.currentTarget.remove();
                                loadItems();
                            } else {
                                toastr.error(response.message);
                            }
                        }).fail(function (error) {
                            console.log(error);
                            $('body').html(error.responseText);
                        });
                    } else {
                        toastr.error('Please correct errors');
                    }
                };
                reader.readAsDataURL(file);
            } else if (e.currentTarget.description.value.length <=300) {    // Validate if a description have less than 300 characters
                $.ajax({
                    url: 'items/' + e.currentTarget.id.value,
                    type: 'PUT',
                    data: {description: e.currentTarget.description.value},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                }).done(function (response) {
                    if (response.result) {
                        toastr.success(response.message);
                        loadItems();
                    } else {
                        toastr.error(response.message);
                    }
                }).fail(function (error) {
                    console.log(error);
                    $('body').html(error.responseText);
                });
            }else{
                toastr.error('The description must have less than 300 characters');
            }
        });
    }else{
        toastr.error('You need save the other item first.')
    }
}

// Validate de image
function changeImage(e){
    var file = e.target.files[0];
    var ext = file.type;
    if(ext == "image/png" || ext == "image/gif" || ext == "image/jpg" || ext == "image/jpeg"){
        img = new Image();
        var imgwidth = 0;
        var imgheight = 0;
        var maxwidth = 320;
        var maxheight = 320;
        e.target.form.getElementsByTagName('img')[0].src = _URL.createObjectURL(file);
        img.src = _URL.createObjectURL(file);
        img.onload = function() {
            imgwidth = this.width;
            imgheight = this.height;
            if(imgwidth <= maxwidth && imgheight <= maxheight){
                imageIsOk = true;
                e.target.form.getElementsByTagName('input')[0].classList.remove('error');
                e.target.form.getElementsByTagName('p')[0].classList.add('hidden');
            }else{
                toastr.error("Image size must be "+maxwidth+"X"+maxheight+" this image have "+imgwidth+"x"+imgheight);
                e.target.form.getElementsByTagName('p')[0].innerHTML = "Image size must be "+maxwidth+"X"+maxheight+" this image have "+imgwidth+"x"+imgheight;
                e.target.form.getElementsByTagName('input')[0].classList.add('error');
                e.target.form.getElementsByTagName('p')[0].classList.remove('hidden');
                imageIsOk = false;
            }
        };
        img.onerror = function() {
            toastr.error("not a valid file: " + file.type);
        }
    }else{
        toastr.error("not a valid file: " + file.type);
    }
}

// Delete item
function deleteItem(e){
    if(e.dataset.id){
        $.ajax({
            url: 'items/'+e.dataset.id,
            type: 'delete',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function (response) {
            if(response.result){
                toastr.success(response.message);
                $('.modal').modal('hide');
                loadItems();
            }else if(response.result===false){
                toastr.error(response.message);
            }else{
                $('body').html(response);
            }
        }).fail(function (error) {
            console.log(error);
            $('body').html(error.responseText);
        });
    }
}

// Change order of item in a list
function changeOrder(id,newList){
    $.ajax({
        url: 'items/'+id+'/change-order',
        type: 'POST',
        data: {newList:newList},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }).done(function (response) {
        toastr.success(response.message);
    }).fail(function (error) {
        console.log(error);
        $('body').html(error.responseText);
    });
}